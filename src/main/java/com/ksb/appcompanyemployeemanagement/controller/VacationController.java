package com.ksb.appcompanyemployeemanagement.controller;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.model.common.CommonResult;
import com.ksb.appcompanyemployeemanagement.model.common.ListResult;
import com.ksb.appcompanyemployeemanagement.model.common.SingleResult;
import com.ksb.appcompanyemployeemanagement.model.vacation.*;
import com.ksb.appcompanyemployeemanagement.service.EmployeeService;
import com.ksb.appcompanyemployeemanagement.service.VacationService;
import com.ksb.appcompanyemployeemanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "연차 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/vacation")
public class VacationController {
    private final VacationService vacationService;
    private final EmployeeService employeeService;

    @ApiOperation(value = "직원 연차 신청서 등록하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true)
    })
    @PostMapping("/new/vacation-history/{employeeId}")
    public CommonResult setVacationHistory(@PathVariable long employeeId, @RequestBody @Valid VacationHistoryCreateRequest request) {
        Employee employee = employeeService.getEmployeeData(employeeId);
        vacationService.setVacationHistory(employee, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 연차 추가하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스",required = true),
            @ApiImplicitParam(name = "plusValue", value = "추가할 값", required = true)
    })
    @PutMapping("/employee/{employeeId}/plus-value/{plusValue}")
    public CommonResult putVacationTotalCount(@PathVariable long employeeId, @PathVariable float plusValue) {
        Employee employee = employeeService.getEmployeeData(employeeId);
        vacationService.putVacationCount(employee, plusValue);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "연차 승인 결재")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vacationHistoryId", value = "연차 내역 시퀀스", required = true),
            @ApiImplicitParam(name = "vacationApproval", value = "연차 승인 여부", required = true)
    })
    @PutMapping("/vacation-history-id/{vacationHistoryId}/vacationApproval/{vacationApproval}")
    public CommonResult putVacationHistoryApproval(@PathVariable long vacationHistoryId, @PathVariable boolean vacationApproval) {
        vacationService.putVacationHistoryApproval(vacationHistoryId, vacationApproval);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "직원 휴가 상태정보 가져오기")
    @GetMapping("/vacation-list/{employeeId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true)
    })
    public ListResult<VacationListItem> getVacationList(@PathVariable long employeeId) {
        return ResponseService.getListResult(vacationService.getVacations(), true);
    }

    @ApiOperation(value = "직원 연차 신청 상세정보 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true)
    })
    @PostMapping("/vacation-history/page/{pageNum}")
    public ListResult<VacationManagerListItem> setVacationListByManager(@PathVariable int pageNum, @RequestBody @Valid VacationSearchRequest request) {
        return ResponseService.getListResult(vacationService.getListByManager(pageNum, request), true);
    }

    @ApiOperation(value = "직원 연차 개수 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/vacation-count/{employeeId}")
    public SingleResult<VacationCountDetail> getVacationCount(@PathVariable long employeeId) {
        return ResponseService.getSingleResult(vacationService.getVacationCount(employeeId));
    }

}
