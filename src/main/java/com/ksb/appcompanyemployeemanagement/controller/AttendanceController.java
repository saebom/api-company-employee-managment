package com.ksb.appcompanyemployeemanagement.controller;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.enums.AttendanceSituation;
import com.ksb.appcompanyemployeemanagement.model.attendance.*;
import com.ksb.appcompanyemployeemanagement.model.common.CommonResult;
import com.ksb.appcompanyemployeemanagement.model.common.SingleResult;
import com.ksb.appcompanyemployeemanagement.service.AttendanceService;
import com.ksb.appcompanyemployeemanagement.service.EmployeeService;
import com.ksb.appcompanyemployeemanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "근태 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
public class AttendanceController {
    private final AttendanceService attendanceService;
    private final EmployeeService employeeService;



    @ApiOperation("근태 상황 출근 등록하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true)
    })
    @PostMapping("/employee/company-in/{employeeId}")
    public CommonResult setAttendance(@PathVariable long employeeId) {
        Employee employee = employeeService.getEmployeeData(employeeId);
        attendanceService.setSituationCompanyIn(employee);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "나의 현재 근태 상황 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/my/attendance-situation/{employeeId}")
    public SingleResult<AttendanceSituationResponse> getAttendance(@PathVariable Long employeeId) {
        return ResponseService.getSingleResult(attendanceService.getMySituation(employeeId));
    }


    @ApiOperation(value = "근태 상황 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true),
            @ApiImplicitParam(name = "attendanceSituation", value = "근태 상태", required = true)
    })
    @PutMapping("/employee/{employeeId}/attendance-situation-change/{attendanceSituation}")
    public CommonResult putAttendanceSituation(@PathVariable long employeeId, @PathVariable AttendanceSituation attendanceSituation) {
        Employee employee = employeeService.getEmployeeData(employeeId);
        attendanceService.putSituation(attendanceSituation, employee);

        return ResponseService.getSuccessResult();
    }

}
