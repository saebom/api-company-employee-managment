package com.ksb.appcompanyemployeemanagement.controller;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.model.common.CommonResult;
import com.ksb.appcompanyemployeemanagement.model.common.ListResult;
import com.ksb.appcompanyemployeemanagement.model.common.SingleResult;
import com.ksb.appcompanyemployeemanagement.model.employee.*;
import com.ksb.appcompanyemployeemanagement.service.EmployeeService;
import com.ksb.appcompanyemployeemanagement.service.VacationService;
import com.ksb.appcompanyemployeemanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "직원 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/employee")
public class EmployeeController {
    private final EmployeeService employeeService;
    private final VacationService annualService;

    @ApiOperation(value = "직원 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setEmployee(@RequestBody @Valid EmployeeJoinRequest request) {
        Employee employee = employeeService.setEmployee(request);
        annualService.setVacation(employee.getId(), employee.getDateIn());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 정보 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<EmployeeItem> getEmployees(@RequestParam(value = "isWork", required = false)Boolean isWork) {
        if (isWork == null) return ResponseService.getListResult(employeeService.getEmployees(), true);
        else return ResponseService.getListResult(employeeService.getEmployees(isWork), true);
    }

    @ApiOperation(value = "직원 상세정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employeeId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/{employeeId}")
    public SingleResult<EmployeeDetail> getEmployee(@PathVariable long employeeId) {
        return ResponseService.getSingleResult(employeeService.getEmployee(employeeId));
    }

    @ApiOperation(value = "사원 로그인하기")
    @PostMapping("/login")
    public SingleResult<EmployeeLoginResponse> doEmployeeLogin(@RequestBody @Valid EmployeeLoginRequest request) {
        return  ResponseService.getSingleResult(employeeService.doLogin(false, request));
    }

    @ApiOperation(value = "관리자 로그인하기")
    @PostMapping("/manager/login")
    public  SingleResult<EmployeeLoginResponse> doManagerLogin(@RequestBody @Valid EmployeeLoginRequest request) {
        return  ResponseService.getSingleResult(employeeService.doLogin(true, request));
    }
}
