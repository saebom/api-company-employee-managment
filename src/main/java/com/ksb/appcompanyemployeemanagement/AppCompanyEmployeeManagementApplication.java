package com.ksb.appcompanyemployeemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCompanyEmployeeManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppCompanyEmployeeManagementApplication.class, args);
    }

}
