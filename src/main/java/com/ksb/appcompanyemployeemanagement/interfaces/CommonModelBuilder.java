package com.ksb.appcompanyemployeemanagement.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
