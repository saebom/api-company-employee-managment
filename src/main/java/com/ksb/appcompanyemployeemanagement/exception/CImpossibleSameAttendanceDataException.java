package com.ksb.appcompanyemployeemanagement.exception;

public class CImpossibleSameAttendanceDataException extends RuntimeException{
    public CImpossibleSameAttendanceDataException(String msg, Throwable t) {super(msg, t);}

    public CImpossibleSameAttendanceDataException(String msg) {
        super(msg);
    }

    public CImpossibleSameAttendanceDataException() {
        super();
    }
}
