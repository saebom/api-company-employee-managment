package com.ksb.appcompanyemployeemanagement.exception;

public class CAlreadyVacationSituationDataException extends RuntimeException{
    public CAlreadyVacationSituationDataException(String msg, Throwable t) {super(msg, t);}

    public CAlreadyVacationSituationDataException(String msg) {
        super(msg);
    }

    public CAlreadyVacationSituationDataException() {
        super();
    }
}
