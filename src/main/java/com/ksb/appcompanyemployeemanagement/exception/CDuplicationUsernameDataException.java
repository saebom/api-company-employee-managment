package com.ksb.appcompanyemployeemanagement.exception;

public class CDuplicationUsernameDataException extends RuntimeException{
    public CDuplicationUsernameDataException(String msg, Throwable t) {super(msg, t);}

    public CDuplicationUsernameDataException(String msg) {
        super(msg);
    }

    public CDuplicationUsernameDataException() {
        super();
    }
}
