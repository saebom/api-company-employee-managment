package com.ksb.appcompanyemployeemanagement.exception;

public class CImpossibleCompanyOutAfterDataException extends RuntimeException{
    public CImpossibleCompanyOutAfterDataException(String msg, Throwable t) {super(msg, t);}

    public CImpossibleCompanyOutAfterDataException(String msg) {
        super(msg);
    }

    public CImpossibleCompanyOutAfterDataException() {
        super();
    }
}
