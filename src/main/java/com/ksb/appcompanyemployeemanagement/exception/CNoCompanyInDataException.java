package com.ksb.appcompanyemployeemanagement.exception;

public class CNoCompanyInDataException extends RuntimeException{
    public CNoCompanyInDataException(String msg, Throwable t) {super(msg, t);}

    public CNoCompanyInDataException(String msg) {
        super(msg);
    }

    public CNoCompanyInDataException() {
        super();
    }
}
