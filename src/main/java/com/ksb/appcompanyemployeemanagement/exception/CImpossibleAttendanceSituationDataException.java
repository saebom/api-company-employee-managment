package com.ksb.appcompanyemployeemanagement.exception;

public class CImpossibleAttendanceSituationDataException extends RuntimeException{
    public CImpossibleAttendanceSituationDataException(String msg, Throwable t) {super(msg, t);}

    public CImpossibleAttendanceSituationDataException(String msg) {
        super(msg);
    }

    public CImpossibleAttendanceSituationDataException() {
        super();
    }
}
