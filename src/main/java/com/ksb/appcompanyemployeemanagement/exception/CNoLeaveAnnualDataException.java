package com.ksb.appcompanyemployeemanagement.exception;

public class CNoLeaveAnnualDataException extends RuntimeException{
    public CNoLeaveAnnualDataException(String msg, Throwable t) {super(msg, t);}

    public CNoLeaveAnnualDataException(String msg) {
        super(msg);
    }

    public CNoLeaveAnnualDataException() {
        super();
    }
}
