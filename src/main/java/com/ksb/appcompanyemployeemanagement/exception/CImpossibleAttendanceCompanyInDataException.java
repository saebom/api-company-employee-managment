package com.ksb.appcompanyemployeemanagement.exception;

public class CImpossibleAttendanceCompanyInDataException extends RuntimeException{
    public CImpossibleAttendanceCompanyInDataException(String msg, Throwable t) {super(msg, t);}

    public CImpossibleAttendanceCompanyInDataException(String msg) {
        super(msg);
    }

    public CImpossibleAttendanceCompanyInDataException() {
        super();
    }
}
