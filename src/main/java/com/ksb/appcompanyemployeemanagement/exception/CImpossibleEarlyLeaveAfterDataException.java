package com.ksb.appcompanyemployeemanagement.exception;

public class CImpossibleEarlyLeaveAfterDataException extends RuntimeException{
    public CImpossibleEarlyLeaveAfterDataException(String msg, Throwable t) {super(msg, t);}

    public CImpossibleEarlyLeaveAfterDataException(String msg) {
        super(msg);
    }

    public CImpossibleEarlyLeaveAfterDataException() {
        super();
    }
}
