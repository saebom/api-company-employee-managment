package com.ksb.appcompanyemployeemanagement.exception;

public class CNoEmployeeDataException extends RuntimeException{
    public CNoEmployeeDataException(String msg, Throwable t) {super(msg, t);}

    public CNoEmployeeDataException(String msg) {
        super(msg);
    }

    public CNoEmployeeDataException() {
        super();
    }
}
