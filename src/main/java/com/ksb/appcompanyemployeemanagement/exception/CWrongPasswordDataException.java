package com.ksb.appcompanyemployeemanagement.exception;

public class CWrongPasswordDataException extends RuntimeException{
    public CWrongPasswordDataException(String msg, Throwable t) {super(msg, t);}

    public CWrongPasswordDataException(String msg) {
        super(msg);
    }

    public CWrongPasswordDataException() {
        super();
    }
}
