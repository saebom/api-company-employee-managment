package com.ksb.appcompanyemployeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmployeeField {
    DEVELOPER("개발팀"),
    MARKETER("마케터"),
    CONTROL("제어팀"),
    FINANCE("재무팀"),
    HR("인사팀");

    private final String name;
}
