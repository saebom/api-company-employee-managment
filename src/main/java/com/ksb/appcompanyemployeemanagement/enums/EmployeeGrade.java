package com.ksb.appcompanyemployeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmployeeGrade {
    GM("부장"),
    DGM("차장"),
    MANAGER("과장"),
    ASSISTANT_MANAGER("대리"),
    SENIOR_STAFF("주임"),
    STAFF("사원");

    private final String name;
}
