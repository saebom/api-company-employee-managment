package com.ksb.appcompanyemployeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AttendanceSituation {
    COMPANY_IN("출근"),
    COMPANY_OUT("퇴근"),
    EARLY_LEAVE("조퇴"),
    NONE("상태 없음");

    private final String name;
}
