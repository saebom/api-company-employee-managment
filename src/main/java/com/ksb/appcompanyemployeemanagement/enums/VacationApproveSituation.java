package com.ksb.appcompanyemployeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VacationApproveSituation {
    APPROVE("승인"),
    WAITING("대기"),
    COMPANION("반려");

    private final String name;
}
