package com.ksb.appcompanyemployeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VacationType {
    HALFWAY("반차"),
    ANNUAL("연차"),
    OTHER("기타");

    private final String name;
}
