package com.ksb.appcompanyemployeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다.")
    ,FAILED(-1, "실패하였습니다.")

    ,MISSING_DATA(-10000,"데이터를 찾을 수 없습니다.")

    ,NO_USERNAME_DATA(-11000,"아이디가 존재하지 않습니다.")
    ,WRONG_PASSWORD_DATA(-12000,"잘못된 비밀번호입니다.")

    ,NO_EMPLOYEE(-20000,"퇴사한 사원입니다. 관리자에 문의해주세요")
    ,NO_COMPANY_IN(-21000, "출근 기록이 없습니다.")
    ,IMPOSSIBLE_ATTENDANCE_AFTER_COMPANY_OUT(-22000,"퇴근 후에는 근태 변경이 불가능합니다.")
    ,IMPOSSIBLE_ATTENDANCE_COMPANY_IN(-23000, "근태를 다시 출근으로 변경이 불가능합니다.")
    ,IMPOSSIBLE_ATTENDANCE_AFTER_EARLY_LEAVE(-24000,"조퇴 후에는 근태 변경이 불가능합니다.")
    ,IMPOSSIBLE_SAME_ATTENDANCE(-25000, "같은 근태 상태로 변경이 불가능합니다.")

    ,IMPOSSIBLE_ATTENDANCE_SITUATION(-30000, "같은 근태상태로 변경이 불가능합니다.")
    ,NO_CURRENT_PASSWORD_CORRECT(-35000, "현재 비밀번호가 일치하지 않습니다.")
    ,NO_CHANGE_PASSWORD_CORRECT(-40000, "변경할 비밀번호가 일치하지않습니다.")
    ,DUPLICATION_USERNAME(-41000, "중복된 ID 입니다.")
    ,ALREADY_VACATION_SITUATION(-50000, "이미 처리된 휴가입니다.")
    ,NO_LEAVE_ANNUAL_COUNT(-51000, "남은 연차가 없습니다.");


    private final Integer code;
    private final String msg;
}
