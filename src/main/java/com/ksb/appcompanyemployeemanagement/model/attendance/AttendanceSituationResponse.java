package com.ksb.appcompanyemployeemanagement.model.attendance;

import com.ksb.appcompanyemployeemanagement.entity.Attendance;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceSituationResponse {
    @ApiModelProperty(notes = "근태 상태")
    private String attendanceSituation;

    private AttendanceSituationResponse(AttendanceSituationResponseBuilder builder) {
        this.attendanceSituation = builder.attendanceSituation;
    }

    public static class AttendanceSituationResponseBuilder implements CommonModelBuilder<AttendanceSituationResponse> {
        private final String attendanceSituation;

        public AttendanceSituationResponseBuilder(Attendance attendance) {
            this.attendanceSituation = attendance.getAttendanceSituation().getName();
        }

        @Override
        public AttendanceSituationResponse build() {
            return new AttendanceSituationResponse(this);
        }
    }
}
