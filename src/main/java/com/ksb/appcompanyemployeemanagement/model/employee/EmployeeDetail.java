package com.ksb.appcompanyemployeemanagement.model.employee;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EmployeeDetail {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "사원 이름")
    private String employeeName;
    @ApiModelProperty(notes = "사원번호")
    private Integer employeeNumber;
    @ApiModelProperty(notes = "아이디")
    private String username;
    @ApiModelProperty(notes = "사원 생년월일")
    private LocalDate employeeBirthday;
    @ApiModelProperty(notes = "사원 부서")
    private String employeeField;
    @ApiModelProperty(notes = "사원 직급")
    private String employeeGrade;
    @ApiModelProperty(notes = "사원 입사일")
    private LocalDate dateIn;

    private EmployeeDetail(EmployeeDetailBuilder builder) {
        this.id = builder.id;
        this.employeeName = builder.employeeName;
        this.employeeNumber = builder.employeeNumber;
        this.username = builder.username;
        this.employeeBirthday = builder.employeeBirthday;
        this.employeeField = builder.employeeField;
        this.employeeGrade = builder.employeeGrade;
        this.dateIn = builder.dateIn;
    }

    public static class EmployeeDetailBuilder implements CommonModelBuilder<EmployeeDetail> {
        private final Long id;
        private final String employeeName;
        private final Integer employeeNumber;
        private final String username;
        private final LocalDate employeeBirthday;
        private final String employeeField;
        private final String employeeGrade;
        private final LocalDate dateIn;

        public EmployeeDetailBuilder(Employee employee) {
            this.id = employee.getId();
            this.employeeName = employee.getEmployeeName();
            this.employeeNumber = employee.getEmployeeNumber();
            this.username = employee.getUsername();
            this.employeeBirthday = employee.getEmployeeBirthday();
            this.employeeField = employee.getEmployeeField().getName();
            this.employeeGrade = employee.getEmployeeGrade().getName();
            this.dateIn = employee.getDateIn();
        }

        @Override
        public EmployeeDetail build() {
            return new EmployeeDetail(this);
        }
    }
}
