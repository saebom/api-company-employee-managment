package com.ksb.appcompanyemployeemanagement.model.employee;

import com.ksb.appcompanyemployeemanagement.enums.EmployeeField;
import com.ksb.appcompanyemployeemanagement.enums.EmployeeGrade;
import com.ksb.appcompanyemployeemanagement.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class EmployeeJoinRequest {
    @ApiModelProperty(notes = "사원명 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String employeeName;

    @ApiModelProperty(notes = "사원 생년월일", required = true)
    @NotNull
    private LocalDate employeeBirthday;

    @ApiModelProperty(notes = "사원 전화번호 (13~13자)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String employeePhone;

    @ApiModelProperty(notes = "사원 성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender employeeGender;

    @ApiModelProperty(notes = "사원 부서", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private EmployeeField employeeField;

    @ApiModelProperty(notes = "사원 직급", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private EmployeeGrade employeeGrade;

    @ApiModelProperty(notes = "사원 번호", required = true)
    @NotNull
    private Integer employeeNumber;

    @ApiModelProperty(notes = "아이디 (8~20자)", required = true)
    @NotNull
    @Length(min = 5, max = 20)
    private String username;

    @ApiModelProperty(notes = "비밀번호 (8~20자)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;

    @ApiModelProperty(notes = "입사일", required = true)
    @NotNull
    private LocalDate dateIn;
}
