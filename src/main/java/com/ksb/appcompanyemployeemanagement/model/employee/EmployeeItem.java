package com.ksb.appcompanyemployeemanagement.model.employee;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EmployeeItem {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "사원명과 직급")
    private String employeeFullName;
    @ApiModelProperty(notes = "사원 부서")
    private String employeeField;
    @ApiModelProperty(notes = "사원 전화번호")
    private String employeePhone;
    @ApiModelProperty(notes = "관리자 여부")
    private String isManager;
    @ApiModelProperty(notes = "근속 여부")
    private String isWork;

    private EmployeeItem(EmployeeItemBuilder builder) {
        this.id = builder.id;
        this.employeeFullName = builder.employeeFullName;
        this.employeeField = builder.employeeField;
        this.employeePhone = builder.employeePhone;
        this.isManager = builder.isManager;
        this.isWork = builder.isWork;
    }

    public static class EmployeeItemBuilder implements CommonModelBuilder<EmployeeItem> {
        private final Long id;
        private final String employeeFullName;
        private final String employeeField;
        private final String employeePhone;
        private final String isManager;
        private final String isWork;

        public EmployeeItemBuilder(Employee employee) {
            this.id = employee.getId();
            this.employeeFullName = employee.getEmployeeName() + employee.getEmployeeGrade().getName();
            this.employeeField = employee.getEmployeeField().getName();
            this.employeePhone = employee.getEmployeePhone();
            this.isManager = employee.getIsManager() ? "관리자" : "-";
            this.isWork = employee.getIsWork() ? "재직" : "퇴사";
        }

        @Override
        public EmployeeItem build() {
            return new EmployeeItem(this);
        }
    }
}
