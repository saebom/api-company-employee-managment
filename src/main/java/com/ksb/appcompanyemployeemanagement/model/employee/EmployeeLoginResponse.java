package com.ksb.appcompanyemployeemanagement.model.employee;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EmployeeLoginResponse {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long employeeId;

    @ApiModelProperty(notes = "사원 생년월일")
    private LocalDate employeeBirthday;

    @ApiModelProperty(notes = "사원 번호")
    private Integer employeeNumber;

    @ApiModelProperty(notes = "아이디")
    private String username;

    @ApiModelProperty(notes = "입사일")
    private LocalDate dateIn;

    private EmployeeLoginResponse(EmployeeLoginResponseBuilder builder) {
        this.employeeId = builder.employeeId;
        this.employeeBirthday = builder.employeeBirthday;
        this.employeeNumber = builder.employeeNumber;
        this.username = builder.username;
        this.dateIn = builder.dateIn;
    }

    public static class EmployeeLoginResponseBuilder implements CommonModelBuilder<EmployeeLoginResponse> {
        private final Long employeeId;
        private final LocalDate employeeBirthday;
        private final Integer employeeNumber;
        private final String username;
        private final LocalDate dateIn;

        public EmployeeLoginResponseBuilder(Employee employee) {
            this.employeeId = employee.getId();
            this.employeeBirthday = employee.getEmployeeBirthday();
            this.employeeNumber = employee.getEmployeeNumber();
            this.username = employee.getUsername();
            this.dateIn = employee.getDateIn();
        }

        @Override
        public EmployeeLoginResponse build() {
            return new EmployeeLoginResponse(this);
        }
    }
}
