package com.ksb.appcompanyemployeemanagement.model.vacation;

import com.ksb.appcompanyemployeemanagement.enums.VacationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class VacationHistoryCreateRequest {
    @ApiModelProperty(notes = "휴가 제목", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String vacationName;

    @ApiModelProperty(notes = "휴가 종류", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private VacationType vacationType;

    @ApiModelProperty(notes = "휴가 날짜", required = true)
    @NotNull
    private LocalDate dateVacation;

    @ApiModelProperty(notes = "휴가 사유", required = true)
    @NotNull
    @Length(max = 50)
    private String vacationReason;
}
