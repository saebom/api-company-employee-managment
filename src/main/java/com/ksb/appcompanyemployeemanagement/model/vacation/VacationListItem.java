package com.ksb.appcompanyemployeemanagement.model.vacation;

import com.ksb.appcompanyemployeemanagement.entity.VacationHistory;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationListItem {
    @ApiModelProperty(notes = "휴가 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "휴가 제목")
    private String vacationName;
    @ApiModelProperty(notes = "휴가 종류")
    private String  vacationType;
    @ApiModelProperty(notes = "휴가 날짜")
    private LocalDate dateVacation;
    @ApiModelProperty(notes = "휴가 사유")
    private String vacationReason;

    private VacationListItem(VacationListItemBuilder builder) {
        this.id = builder.id;
        this.vacationName = builder.vacationName;
        this.vacationType = builder.vacationType;
        this.dateVacation = builder.dateVacation;
        this.vacationReason = builder.vacationReason;
    }


    public static class  VacationListItemBuilder implements CommonModelBuilder<VacationListItem> {
        private final Long id;
        private final String vacationName;
        private final String vacationType;
        private final LocalDate dateVacation;
        private final String vacationReason;

        public VacationListItemBuilder(VacationHistory vacationHistory) {
            this.id = vacationHistory.getId();
            this.vacationName = vacationHistory.getVacationName();
            this.vacationType = vacationHistory.getVacationType().getName();
            this.dateVacation = vacationHistory.getDateVacation();
            this.vacationReason = vacationHistory.getVacationReason();
        }

        @Override
        public VacationListItem build() {
            return new VacationListItem(this);
        }
    }
}
