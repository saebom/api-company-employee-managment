package com.ksb.appcompanyemployeemanagement.model.vacation;

import com.ksb.appcompanyemployeemanagement.entity.VacationCount;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationCountDetail {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long employeeId;
    @ApiModelProperty(notes = "총 연차수")
    private String totalVacationCount;
    @ApiModelProperty(notes = "사용 연차수")
    private String usageVacationCount;
    @ApiModelProperty(notes = "남은 연차수")
    private String leaveVacationCount;
    @ApiModelProperty(notes = "연차 시작일")
    private LocalDate dateVacationCountStart;
    @ApiModelProperty(notes = "연차 종료일")
    private LocalDate dateVacationCountEnd;

    private VacationCountDetail(VacationCountDetailBuilder builder) {
        this.employeeId = builder.employeeId;
        this.totalVacationCount = builder.totalVacationCount;
        this.usageVacationCount = builder.usageVacationCount;
        this.leaveVacationCount = builder.leaveVacationCount;
        this.dateVacationCountStart = builder.dateVacationCountStart;
        this.dateVacationCountEnd = builder.dateVacationCountEnd;
    }

    public static class VacationCountDetailBuilder implements CommonModelBuilder<VacationCountDetail> {
        private final Long employeeId;
        private final String totalVacationCount;
        private final String usageVacationCount;
        private final String leaveVacationCount;
        private final LocalDate dateVacationCountStart;
        private final LocalDate dateVacationCountEnd;

        public VacationCountDetailBuilder(VacationCount vacationCount) {
            this.employeeId = vacationCount.getEmployeeId();
            this.totalVacationCount = vacationCount.getTotalVacationCount() + "일";
            this.usageVacationCount = vacationCount.getUsageVacationCount() + "일";
            this.leaveVacationCount = vacationCount.getLeaveVacationCount() + "일";
            this.dateVacationCountStart = vacationCount.getDateVacationCountStart();
            this.dateVacationCountEnd = vacationCount.getDateVacationCountEnd();
        }

        @Override
        public VacationCountDetail build() {
            return new VacationCountDetail(this);
        }
    }
}
