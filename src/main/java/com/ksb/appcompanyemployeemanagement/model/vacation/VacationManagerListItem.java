package com.ksb.appcompanyemployeemanagement.model.vacation;

import com.ksb.appcompanyemployeemanagement.entity.VacationHistory;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class VacationManagerListItem {
    @ApiModelProperty(notes = "휴가 내역 시퀀스")
    private Long vacationHistoryId;
    @ApiModelProperty(notes = "사원 분야 및 사원명과 직급")
    private String employeeFullName;
    @ApiModelProperty(notes = "휴가 제목")
    private String vacationName;
    @ApiModelProperty(notes = "휴가 종류")
    private String vacationType;
    @ApiModelProperty(notes = "휴가 날짜")
    private LocalDate dateVacation;
    @ApiModelProperty(notes = "휴가 사유")
    private String vacationReason;
    @ApiModelProperty(notes = "휴가 허락 상황")
    private String vacationApproveSituation;

    private VacationManagerListItem(VacationManagerListItemBuilder builder) {
        this.vacationHistoryId = builder.vacationHistoryId;
        this.employeeFullName = builder.employeeFullName;
        this.vacationName = builder.vacationName;
        this.vacationType = builder.vacationType;
        this.dateVacation = builder.dateVacation;
        this.vacationReason = builder.vacationReason;
        this.vacationApproveSituation = builder.vacationApproveSituation;
    }

    public static class VacationManagerListItemBuilder implements CommonModelBuilder<VacationManagerListItem> {
        private final Long vacationHistoryId;
        private final String employeeFullName;
        private final String vacationName;
        private final String vacationType;
        private final LocalDate dateVacation;
        private final String vacationReason;
        private final String vacationApproveSituation;

        public VacationManagerListItemBuilder(VacationHistory vacationHistory) {
            this.vacationHistoryId = vacationHistory.getId();
            this.employeeFullName = "[" + vacationHistory.getEmployee().getEmployeeField().getName() + "]" + vacationHistory.getEmployee().getEmployeeName() + vacationHistory.getEmployee().getEmployeeGrade().getName();
            this.vacationName = vacationHistory.getVacationName();
            this.vacationType = vacationHistory.getVacationType().getName();
            this.dateVacation = vacationHistory.getDateVacation();
            this.vacationReason = vacationHistory.getVacationReason();
            this.vacationApproveSituation = vacationHistory.getVacationApproveSituation().getName();
        }

        @Override
        public VacationManagerListItem build() {
            return new VacationManagerListItem(this);
        }
    }

}
