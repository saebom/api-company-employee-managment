package com.ksb.appcompanyemployeemanagement.model.vacation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class VacationSearchRequest {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long employeeId;

    @ApiModelProperty(notes = "시작일", required = true)
    @NotNull
    private LocalDate dateStart;

    @ApiModelProperty(notes = "종료일", required = true)
    @NotNull
    private LocalDate dateEnd;
}
