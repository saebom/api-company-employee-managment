package com.ksb.appcompanyemployeemanagement.advice;

import com.ksb.appcompanyemployeemanagement.enums.ResultCode;
import com.ksb.appcompanyemployeemanagement.exception.*;
import com.ksb.appcompanyemployeemanagement.model.common.CommonResult;
import com.ksb.appcompanyemployeemanagement.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CNoUsernameDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoUsernameDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_USERNAME_DATA);
    }

    @ExceptionHandler(CWrongPasswordDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPasswordDataException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PASSWORD_DATA);
    }

    @ExceptionHandler(CNoEmployeeDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoEmployeeDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_EMPLOYEE);
    }

    @ExceptionHandler(CNoCompanyInDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoCompanyInDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_COMPANY_IN);
    }

    @ExceptionHandler(CImpossibleCompanyOutAfterDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleCompanyOutAfterDataException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_ATTENDANCE_AFTER_COMPANY_OUT);
    }

    @ExceptionHandler(CImpossibleAttendanceCompanyInDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleAttendanceCompanyInDataException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_ATTENDANCE_COMPANY_IN);
    }

    @ExceptionHandler(CImpossibleEarlyLeaveAfterDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleEarlyLeaveAfterDataException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_ATTENDANCE_AFTER_EARLY_LEAVE);
    }

    @ExceptionHandler(CImpossibleSameAttendanceDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleSameAttendanceDataException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_SAME_ATTENDANCE);
    }

    @ExceptionHandler(CImpossibleAttendanceSituationDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleAttendanceSituationDataException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_ATTENDANCE_SITUATION);
    }

    @ExceptionHandler(CAlreadyVacationSituationDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyVacationSituationDataException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_VACATION_SITUATION);
    }

    @ExceptionHandler(CNoLeaveAnnualDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoLeaveAnnualDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_LEAVE_ANNUAL_COUNT);
    }

    @ExceptionHandler(CDuplicationUsernameDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDuplicationUsernameDataException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATION_USERNAME);
    }
}
