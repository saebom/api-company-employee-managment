package com.ksb.appcompanyemployeemanagement.service;

import com.ksb.appcompanyemployeemanagement.entity.Attendance;
import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.enums.AttendanceSituation;
import com.ksb.appcompanyemployeemanagement.exception.*;
import com.ksb.appcompanyemployeemanagement.model.attendance.*;
import com.ksb.appcompanyemployeemanagement.repository.AttendanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;

    /**
     * 근태 상황 가져오기
     * @param employeeId 직원 아이디
     * @return 근태상황
     */
    public AttendanceSituationResponse getMySituation(long employeeId) {
        Attendance attendance = attendanceRepository.findAllByDateReferenceAndEmployee_Id(LocalDate.now(), employeeId).orElse(new Attendance.AttendanceNoneValueBuilder().build());
        return new AttendanceSituationResponse.AttendanceSituationResponseBuilder(attendance).build();
    }

    /**
     * 나의 근태 상태 등록
     * @param employee  직원 entity
     */
    public void setSituationCompanyIn(Employee employee) {
        Optional<Attendance> attendance = attendanceRepository.findAllByDateReferenceAndEmployee_Id(LocalDate.now(), employee.getId());

        if (attendance.isPresent())throw new CImpossibleAttendanceSituationDataException();

        Attendance addAttendance = new Attendance.AttendanceBuilder(employee).build();
        attendanceRepository.save(addAttendance);
    }

    /**
     * 나의 근태 상태 수정
     * @param attendanceSituation 근태 상태
     * @param employee 직원 entity
     */
    public void putSituation(AttendanceSituation attendanceSituation, Employee employee) {
        Optional<Attendance> attendance = attendanceRepository.findAllByDateReferenceAndEmployee_Id(LocalDate.now(), employee.getId());

        if (attendance.isEmpty())throw new CNoCompanyInDataException();
        if (attendance.get().getAttendanceSituation().equals(AttendanceSituation.COMPANY_OUT))throw new CImpossibleCompanyOutAfterDataException();
        if (attendanceSituation.equals(AttendanceSituation.COMPANY_IN))throw new CImpossibleAttendanceCompanyInDataException();
        if (attendance.get().getAttendanceSituation().equals(AttendanceSituation.EARLY_LEAVE))throw new CImpossibleEarlyLeaveAfterDataException();
        if (attendanceSituation.equals(attendance.get().getAttendanceSituation()))throw new CImpossibleSameAttendanceDataException();
        Attendance targetAttendance = attendance.get();
        targetAttendance.putSituation(attendanceSituation);
        attendanceRepository.save(targetAttendance);
    }


}
