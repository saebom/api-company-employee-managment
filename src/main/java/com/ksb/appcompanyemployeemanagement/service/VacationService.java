package com.ksb.appcompanyemployeemanagement.service;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.entity.VacationCount;
import com.ksb.appcompanyemployeemanagement.entity.VacationHistory;
import com.ksb.appcompanyemployeemanagement.enums.VacationApproveSituation;
import com.ksb.appcompanyemployeemanagement.enums.VacationType;
import com.ksb.appcompanyemployeemanagement.exception.CAlreadyVacationSituationDataException;
import com.ksb.appcompanyemployeemanagement.exception.CMissingDataException;
import com.ksb.appcompanyemployeemanagement.exception.CNoLeaveAnnualDataException;
import com.ksb.appcompanyemployeemanagement.model.common.ListResult;
import com.ksb.appcompanyemployeemanagement.model.vacation.*;
import com.ksb.appcompanyemployeemanagement.repository.VacationCountRepository;
import com.ksb.appcompanyemployeemanagement.repository.VacationHistoryRepository;
import com.ksb.appcompanyemployeemanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VacationService {
    private final VacationCountRepository vacationCountRepository;
    private final VacationHistoryRepository vacationHistoryRepository;

    /**
     * 직원 휴가 등록
     * @param employeeId 직원 시퀀스
     * @param dateIn 입사일
     */
    public void setVacation(long employeeId, LocalDate dateIn) {
        VacationCount vacationCount = new VacationCount.VacationCountBuilder(employeeId, dateIn).build();
        vacationCountRepository.save(vacationCount);
    }

    /**
     * 직원 휴가 신청
     * @param employee 직원 entity
     * @param request 직원이 휴가 신청을 위한 정보(model)
     */
    public void setVacationHistory(Employee employee, VacationHistoryCreateRequest request) {
        VacationHistory vacationHistory = new VacationHistory.VacationHistoryBuilder(employee, request).build();
        vacationHistoryRepository.save(vacationHistory);
    }

    /**
     * 휴가 승인 결재 기능
     * @param vacationHistoryId 휴가 내역 시퀀스
     * @param vacationApproval 휴가 승인 여부
     */
    public void putVacationHistoryApproval(long vacationHistoryId,boolean vacationApproval) {
        VacationHistory vacationHistory = vacationHistoryRepository.findById(vacationHistoryId).orElseThrow(CMissingDataException::new);

        if (!vacationHistory.getVacationApproveSituation().equals(VacationApproveSituation.WAITING))throw new CAlreadyVacationSituationDataException();

        if (vacationApproval) {
            VacationCount vacationCount = vacationCountRepository.findAllByEmployeeId(vacationHistory.getEmployee().getId()).orElseThrow(CMissingDataException::new);

            float plusValue = vacationHistory.getVacationType() == VacationType.ANNUAL ? 1f : 0.5f;

            if (plusValue > vacationCount.getLeaveVacationCount())throw new CNoLeaveAnnualDataException();
            vacationCount.putUsageVacationCount(plusValue);
            vacationCountRepository.save(vacationCount);

            vacationHistory.putIncreaseOrDecreaseValue(plusValue);
        }
        vacationHistory.putVacationApproval(vacationApproval);
        vacationHistoryRepository.save(vacationHistory);
    }

    /**
     * 직원 리스트 가져오기
     * @return 직원 휴가 리스트를 가져오기 위한 정보(model)
     */
    public ListResult<VacationListItem> getVacations() {
        List<VacationListItem> result = new LinkedList<>();

        List<VacationHistory> vacationHistories = vacationHistoryRepository.findAll();

        vacationHistories.forEach(vacationHistory -> {
            VacationListItem vacationListItem = new VacationListItem.VacationListItemBuilder(vacationHistory).build();
            result.add(vacationListItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 휴가 일 수 수정
     * @param employee 직원 entity
     * @param plusValue 휴가 증가, 감소
     */
    public void putVacationCount(Employee employee, float plusValue) {
        VacationCount vacationCount = vacationCountRepository.findAllByEmployeeId(employee.getId()).orElseThrow(CMissingDataException::new);

        VacationHistory vacationHistory = new VacationHistory.VacationHistoryPlusAnnualBuilder(employee, plusValue).build();
        vacationHistoryRepository.save(vacationHistory);

        vacationCount.putTotalVacationCount(plusValue);
        vacationCountRepository.save(vacationCount);
    }


    /**
     * 직원 연차 신청 리스트 조회
     * @param pageNum 페이지번호
     * @param searchRequest 조회를 하기 위한 시작일,종료일, 사원 시퀀스 정보
     * @return 연차 신청을 위한 정보(model)
     */
    public ListResult<VacationManagerListItem> getListByManager(int pageNum, VacationSearchRequest searchRequest) {
        Page<VacationHistory> vacationHistories;
        PageRequest pageRequest = PageRequest.of(pageNum, 10);

        if (searchRequest.getEmployeeId() == null) {
            vacationHistories = vacationHistoryRepository.findAllByDateVacationGreaterThanEqualAndDateVacationLessThanEqualOrderByDateVacationAsc(searchRequest.getDateStart(), searchRequest.getDateEnd(), pageRequest);
        }else {
            vacationHistories = vacationHistoryRepository.findAllByEmployee_IdAndDateVacationGreaterThanEqualAndDateVacationLessThanEqualOrderByDateVacationAsc(searchRequest.getEmployeeId(), searchRequest.getDateStart(), searchRequest.getDateEnd(), pageRequest);
        }

        List<VacationManagerListItem> result = new LinkedList<>();

        vacationHistories.forEach(vacationHistory -> {
            VacationManagerListItem vacationManagerListItem = new VacationManagerListItem.VacationManagerListItemBuilder(vacationHistory).build();
            result.add(vacationManagerListItem);
        });

        return ListConvertService.settingResult(result, vacationHistories.getTotalElements(), vacationHistories.getTotalPages(), vacationHistories.getPageable().getPageNumber());
    }

    /**
     * 직원 연차 개수 정보 가져오기
     * @param employeeId 사원 시퀀스
     * @return 직원 연차 개수를 가져오기 위한 정보(model)
     */
    public VacationCountDetail getVacationCount(long employeeId) {
        VacationCount vacationCount = vacationCountRepository.findById(employeeId).orElseThrow(CMissingDataException::new);
        return new VacationCountDetail.VacationCountDetailBuilder(vacationCount).build();
    }

}
