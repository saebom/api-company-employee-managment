package com.ksb.appcompanyemployeemanagement.service;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import com.ksb.appcompanyemployeemanagement.exception.*;
import com.ksb.appcompanyemployeemanagement.model.common.ListResult;
import com.ksb.appcompanyemployeemanagement.model.employee.*;
import com.ksb.appcompanyemployeemanagement.repository.EmployeeRepository;
import com.ksb.appcompanyemployeemanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    /**
     * 직원 등록하기.
     * @param request 직원을 등록할때 필요 정보
     * @return 직원 정보
     */
    public Employee setEmployee(EmployeeJoinRequest request) {
        if (duplicationUsername(request.getUsername()))throw new CDuplicationUsernameDataException();
        Employee employee = new Employee.EmployeeBuilder(request).build();
        return employeeRepository.save(employee);
    }


    public Employee getEmployeeData(long id) {
        return employeeRepository.findById(id).orElseThrow(CNoEmployeeDataException::new);
    }

    /**
     * 직원 정보 가져오기
     * @return
     */
    public ListResult<EmployeeItem> getEmployees() {
        List<EmployeeItem> result = new LinkedList<>();

        List<Employee> employees = employeeRepository.findAll();

        employees.forEach(employee -> {
            EmployeeItem employeeItem = new EmployeeItem.EmployeeItemBuilder(employee).build();
            result.add(employeeItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 직원 상세정보 가져오기
     * @param id 사원 시퀀스
     * @return 직원 상세정보
     */
    public EmployeeDetail getEmployee(long id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new EmployeeDetail.EmployeeDetailBuilder(employee).build();
    }


    /**
     * 직원 정보 리스트 가져오기
     * @param isWork 근속여부
     * @return 근속여부로 리스트 확인
     */
    public ListResult<EmployeeItem> getEmployees(Boolean isWork) {
        List<Employee> employees = employeeRepository.findAllByIsWorkOrderByIdDesc(isWork);

        List<EmployeeItem> result = new LinkedList<>();

        employees.forEach(employee -> {
            EmployeeItem employeeItem = new EmployeeItem.EmployeeItemBuilder(employee).build();
            result.add(employeeItem);
        });

        return ListConvertService.settingResult(result);
    }
    /**
     * 로그인 기능
     * @param isManager 관리자 여부
     * @param request 고객에게 로그인 정보를 받음.
     * @return 직원 정보
     */
    public EmployeeLoginResponse doLogin(boolean isManager, EmployeeLoginRequest request) {
        Employee employee = employeeRepository.findAllByUsernameAndIsManager(request.getUsername(), isManager).orElseThrow(CNoUsernameDataException::new);

        if (!employee.getPassword().equals(request.getPassword()))throw new CWrongPasswordDataException();

        if (!employee.getIsWork())throw new CNoEmployeeDataException();

        return new EmployeeLoginResponse.EmployeeLoginResponseBuilder(employee).build();
    }


    /**
     * 아이디 중복 확인
     * @param username 아이디
     * @return 아이디 중복이면 true, 아이디 중복이 아니면 false
     */
    public boolean duplicationUsername(String username) {
        long usernameCount = employeeRepository.countByUsername(username);
        return usernameCount >= 1;
    }

}
