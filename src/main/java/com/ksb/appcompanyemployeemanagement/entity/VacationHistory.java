package com.ksb.appcompanyemployeemanagement.entity;

import com.ksb.appcompanyemployeemanagement.enums.VacationApproveSituation;
import com.ksb.appcompanyemployeemanagement.enums.VacationType;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import com.ksb.appcompanyemployeemanagement.model.vacation.VacationHistoryCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId", nullable = false)
    private Employee employee;

    @Column(nullable = false)
    private Boolean isMinus;

    @Column(nullable = false)
    private Float increaseOrDecreaseValue;

    @Column(nullable = false, length = 20)
    private String vacationName;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private VacationType vacationType;

    @Column(nullable = false)
    private LocalDate dateVacation;

    @Column(nullable = false, length = 50)
    private String vacationReason;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private VacationApproveSituation vacationApproveSituation;

    private Boolean vacationApproval;

    private LocalDateTime dateApproval;

    private LocalDateTime dateCompanion;

    @Column(nullable = false)
    private LocalDateTime dateRequest;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putIncreaseOrDecreaseValue(float increaseOrDecreaseValue) {
        this.increaseOrDecreaseValue = increaseOrDecreaseValue;
    }

    public void putVacationApproval(boolean vacationApproval) {
        if (vacationApproval) {
            this.vacationApproveSituation = VacationApproveSituation.APPROVE;
            this.dateApproval = LocalDateTime.now();
        }else {
            this.vacationApproveSituation = VacationApproveSituation.COMPANION;
            this.dateCompanion = LocalDateTime.now();
        }
    }

    private VacationHistory(VacationHistoryBuilder builder) {
        this.employee = builder.employee;
        this.isMinus = builder.isMinus;
        this.increaseOrDecreaseValue = builder.increaseOrDecreaseValue;
        this.vacationName = builder.vacationName;
        this.vacationType = builder.vacationType;
        this.dateVacation = builder.dateVacation;
        this.vacationReason = builder.vacationReason;
        this.vacationApproveSituation = builder.vacationApproveSituation;
        this.dateRequest = builder.dateRequest;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class VacationHistoryBuilder implements CommonModelBuilder<VacationHistory> {
        private final Employee employee;
        private final Boolean isMinus;
        private final Float increaseOrDecreaseValue;
        private final String vacationName;
        private final VacationType vacationType;
        private final LocalDate dateVacation;
        private final String vacationReason;
        private final VacationApproveSituation vacationApproveSituation;
        private final LocalDateTime dateRequest;
        private final LocalDateTime dateUpdate;

        public VacationHistoryBuilder(Employee employee, VacationHistoryCreateRequest request) {
            this.employee = employee;
            this.isMinus = true;
            this.increaseOrDecreaseValue = 0f;
            this.vacationName = request.getVacationName();
            this.vacationType = request.getVacationType();
            this.dateVacation = request.getDateVacation();
            this.vacationReason = request.getVacationReason();
            this.vacationApproveSituation = VacationApproveSituation.WAITING;
            this.dateRequest = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public VacationHistory build() {
            return new VacationHistory(this);
        }
    }

    private VacationHistory(VacationHistoryPlusAnnualBuilder builder) {
        this.employee = builder.employee;
        this.isMinus = builder.isMinus;
        this.increaseOrDecreaseValue = builder.increaseOrDecreaseValue;
        this.vacationName = builder.vacationName;
        this.vacationType = builder.vacationType;
        this.dateVacation = builder.dateVacation;
        this.vacationReason = builder.vacationReason;
        this.vacationApproveSituation = builder.vacationApproveSituation;
        this.dateApproval = builder.dateApproval;
        this.dateRequest = builder.dateRequest;
        this.dateUpdate = builder.dateUpdate;
    }


    public static class VacationHistoryPlusAnnualBuilder implements CommonModelBuilder<VacationHistory> {
        private final Employee employee;
        private final Boolean isMinus;
        private final Float increaseOrDecreaseValue;
        private final String vacationName;
        private final VacationType vacationType;
        private final LocalDate dateVacation;
        private final String vacationReason;
        private final VacationApproveSituation vacationApproveSituation;
        private final LocalDateTime dateApproval;
        private final LocalDateTime dateRequest;
        private final LocalDateTime dateUpdate;

        public VacationHistoryPlusAnnualBuilder(Employee employee, float increaseOrDecreaseValue) {
            this.employee = employee;
            this.isMinus = false;
            this.increaseOrDecreaseValue = increaseOrDecreaseValue;
            this.vacationName = "연차 추가";
            this.vacationType = VacationType.ANNUAL;
            this.dateVacation = LocalDate.now();
            this.vacationReason = "연차 추가";
            this.vacationApproveSituation = VacationApproveSituation.APPROVE;
            this.dateApproval = LocalDateTime.now();
            this.dateRequest = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public VacationHistory build() {
            return new VacationHistory(this);
        }
    }
}
