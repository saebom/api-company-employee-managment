package com.ksb.appcompanyemployeemanagement.entity;

import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationCount {
    @Id
    private Long employeeId;

    @Column(nullable = false)
    private Float totalVacationCount;

    @Column(nullable = false)
    private Float usageVacationCount;

    @Column(nullable = false)
    private LocalDate dateVacationCountStart;

    @Column(nullable = false)
    private LocalDate dateVacationCountEnd;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public Float getLeaveVacationCount() {
        return totalVacationCount - usageVacationCount;
    }

    public void putTotalVacationCount(float plusCount) {
        this.totalVacationCount += plusCount;
    }

    public void putUsageVacationCount(float plusCount) {
        this.usageVacationCount += plusCount;
    }

    private VacationCount(VacationCountBuilder builder) {
        this.employeeId = builder.employeeId;
        this.totalVacationCount = builder.totalVacationCount;
        this.usageVacationCount = builder.usageVacationCount;
        this.dateVacationCountStart = builder.dateVacationCountStart;
        this.dateVacationCountEnd = builder.dateVacationCountEnd;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class VacationCountBuilder implements CommonModelBuilder<VacationCount> {
        private final Long employeeId;
        private final Float totalVacationCount;
        private final Float usageVacationCount;
        private final LocalDate dateVacationCountStart;
        private final LocalDate dateVacationCountEnd;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public VacationCountBuilder(Long employeeId, LocalDate dateIn) {
            this.employeeId = employeeId;
            this.totalVacationCount = 0f;
            this.usageVacationCount = 0f;
            this.dateVacationCountStart = dateIn;
            this.dateVacationCountEnd = dateIn.plusYears(1);
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }



        @Override
        public VacationCount build() {
            return new VacationCount(this);
        }
    }
}
