package com.ksb.appcompanyemployeemanagement.entity;

import com.ksb.appcompanyemployeemanagement.enums.EmployeeField;
import com.ksb.appcompanyemployeemanagement.enums.EmployeeGrade;
import com.ksb.appcompanyemployeemanagement.enums.Gender;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import com.ksb.appcompanyemployeemanagement.model.employee.EmployeeJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String employeeName;

    @Column(nullable = false)
    private LocalDate employeeBirthday;

    @Column(nullable = false, length = 20)
    private String employeePhone;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Gender employeeGender;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private EmployeeField employeeField;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private EmployeeGrade employeeGrade;

    @Column(nullable = false)
    private Integer employeeNumber;

    @Column(nullable = false, length = 20,unique = true)
    private String username;

    @Column(nullable = false, length = 20, unique = true)
    private String password;

    @Column(nullable = false)
    private Boolean isManager;

    @Column(nullable = false)
    private Boolean isWork;

    @Column(nullable = false)
    private LocalDate dateIn;

    private LocalDate dateOut;

    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putPassword(String password) {
        this.password = password;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putEmployeeWork() {
        this.isWork = false;
        this.dateOut = LocalDate.now();
    }


    private Employee(EmployeeBuilder builder) {
        this.employeeName = builder.employeeName;
        this.employeeBirthday = builder.employeeBirthday;
        this.employeePhone = builder.employeePhone;
        this.employeeGender = builder.employeeGender;
        this.employeeField = builder.employeeField;
        this.employeeGrade = builder.employeeGrade;
        this.employeeNumber = builder.employeeNumber;
        this.username = builder.username;
        this.password = builder.password;
        this.isManager = builder.isManager;
        this.isWork = builder.isWork;
        this.dateIn = builder.dateIn;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class EmployeeBuilder implements CommonModelBuilder<Employee> {
        private final String employeeName;
        private final LocalDate employeeBirthday;
        private final String employeePhone;
        private final Gender employeeGender;
        private final EmployeeField employeeField;
        private final EmployeeGrade employeeGrade;
        private final Integer employeeNumber;
        private final String username;
        private final String password;
        private final Boolean isManager;
        private final Boolean isWork;
        private final LocalDate dateIn;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public EmployeeBuilder(EmployeeJoinRequest request) {
            this.employeeName = request.getEmployeeName();
            this.employeeBirthday = request.getEmployeeBirthday();
            this.employeePhone = request.getEmployeePhone();
            this.employeeGender = request.getEmployeeGender();
            this.employeeField = request.getEmployeeField();
            this.employeeGrade = request.getEmployeeGrade();
            this.employeeNumber = request.getEmployeeNumber();
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.isManager = false;
            this.isWork = true;
            this.dateIn = request.getDateIn();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Employee build() {
            return new Employee(this);
        }
    }
}
