package com.ksb.appcompanyemployeemanagement.entity;

import com.ksb.appcompanyemployeemanagement.enums.AttendanceSituation;
import com.ksb.appcompanyemployeemanagement.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId", nullable = false)
    private Employee employee;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private AttendanceSituation attendanceSituation;

    @Column(nullable = false)
    private LocalDate dateReference;

    @Column(nullable = false)
    private LocalTime timeCompanyIn;

    private LocalTime timeCompanyOut;

    private LocalTime timeEarlyLeave;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putSituation(AttendanceSituation attendanceSituation) {
        this.attendanceSituation = attendanceSituation;

        switch (attendanceSituation) {
            case COMPANY_OUT:
                this.timeCompanyOut = LocalTime.now();
                break;
            case EARLY_LEAVE:
                this.timeEarlyLeave = LocalTime.now();
                break;
        }

    }

    private Attendance(AttendanceBuilder builder) {
        this.employee = builder.employee;
        this.attendanceSituation = builder.attendanceSituation;
        this.dateReference= builder.dateReference;
        this.timeCompanyIn = builder.timeCompanyIn;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class AttendanceBuilder implements CommonModelBuilder<Attendance> {
        private final Employee employee;
        private final AttendanceSituation attendanceSituation;
        private final LocalDate dateReference;
        private final LocalTime timeCompanyIn;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public AttendanceBuilder(Employee employee) {
            this.employee = employee;
            this.attendanceSituation = AttendanceSituation.COMPANY_IN;
            this.dateReference = LocalDate.now();
            this.timeCompanyIn = LocalTime.now();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }

    private Attendance(AttendanceNoneValueBuilder builder) {
        this.attendanceSituation = builder.attendanceSituation;
    }

    public static class AttendanceNoneValueBuilder implements CommonModelBuilder<Attendance> {
        private final AttendanceSituation attendanceSituation;

        public AttendanceNoneValueBuilder() {
            this.attendanceSituation = AttendanceSituation.NONE;
        }

        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }
}
