package com.ksb.appcompanyemployeemanagement.repository;

import com.ksb.appcompanyemployeemanagement.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findAllByUsernameAndIsManager(String username, Boolean isManager);
    List<Employee> findAllByIsWorkOrderByIdDesc(Boolean isWork);
    long countByUsername(String username);
}
