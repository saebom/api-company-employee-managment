package com.ksb.appcompanyemployeemanagement.repository;

import com.ksb.appcompanyemployeemanagement.entity.VacationCount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VacationCountRepository extends JpaRepository<VacationCount, Long> {
    Optional<VacationCount> findAllByEmployeeId(Long employeeId);
}
