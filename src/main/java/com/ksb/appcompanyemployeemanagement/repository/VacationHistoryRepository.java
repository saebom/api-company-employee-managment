package com.ksb.appcompanyemployeemanagement.repository;

import com.ksb.appcompanyemployeemanagement.entity.VacationHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface VacationHistoryRepository extends JpaRepository<VacationHistory, Long> {
    Page<VacationHistory>
    findAllByDateVacationGreaterThanEqualAndDateVacationLessThanEqualOrderByDateVacationAsc
            (LocalDate dateStat, LocalDate dateEnd, Pageable pageable);
    Page<VacationHistory>
    findAllByEmployee_IdAndDateVacationGreaterThanEqualAndDateVacationLessThanEqualOrderByDateVacationAsc
            (Long employeeId, LocalDate dateStat, LocalDate dateEnd, Pageable pageable);

}
