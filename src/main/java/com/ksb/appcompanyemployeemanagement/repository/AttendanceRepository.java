package com.ksb.appcompanyemployeemanagement.repository;

import com.ksb.appcompanyemployeemanagement.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    Optional<Attendance> findAllByDateReferenceAndEmployee_Id(LocalDate dateReference, long employeeId);

}
