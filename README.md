#  사원관리 API


### LANGUAGE
```
- JAVA16
- SpringBoot 2.7.3
```


### 기능
```
* 직원관리

- 직원 등록하기
- 직원 정보 가져오기
- 직원 상제정보 가져오기
- 직원 근속여부를 통해 정보 가져오기
- 로그인 기능


* 근태관리

- 근태 상황 가져오기    
- 나의 근태 상태 등록하기
- 나의 근태 상태 수정하기

* 휴가관리

- 직원 휴가 등록
- 직원 휴가 신청
- 직원 승인 결재 기능
- 직원 리스트 가져오기
- 직원 휴가 개수 수정
- 직원 연차 신청 상세내역 조회
- 직원 연차 개수 정보 가져오기 

```

### 예시
> * 스웨거 전체 화면 

![swagger_all](./images/swagger.all.png)

> * 스웨거 직원 관리

![swagger_employee](./images/swagger.employee.png)

> * 스웨거 근태 관리

![swagger_attendance](./images/swagger.attendance.png)

> * 스웨거 연차 관리

![swagger_vacation](./images/swagger.vacation.png)